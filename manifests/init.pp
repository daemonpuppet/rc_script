# @summary Create BSD style rc.d scripts
#
# This defined type helps create BSD style rc.d scripts which then can be used
# by a `service`.
#
# @param name
#   This is the name under which the program will be run as daemon, and known to rcorder.
#
# @param ensure
#   Whether to create or remove this rc scripts
#
# @param description
#   This is the description of the daemon, defaults to "${name} daemon"
#
# @param command
#   Theoretically, the only parameter needed: Path to the program this rc script should run
#
# @param command_args
#   Optional arguments to the `command`
#
# @param user
#   The user under which this program should run.
#
# @param group
#   The group under which this program should run.
#
# @param daemon_class
#   FreeBSD and OpenBSD only: The `login.conf(5)` class under which this daemon should run.
#   See https://man.freebsd.org/login.conf and https://man.openbsd.org/login.conf.5
#
# @param limits
#   FreeBSD only: The [`limits(1)`](https://man.freebsd.org/limits) that should be applied to this service. By default they are taken from the login class.
#
# @param requires
#   List of daemons (or meta-services) this rc script depends on.
#
# @param env
#   A Hash of environment variables to export
#
# @param cwd
#   Directory to change into before starting the daemon
#
# @param daemonize
#   FreeBSD only: Whether to use `daemon(8)` to help daemonize this program
#
# @param restart_delay
#    FreeBSD only: When run by `daemon(8)` restart program if it shuts down unexpectedly, after this many seconds
#
# @param use_syslog
#    FreeBSD only: When run by `daemon(8)` send stdout / stderr to syslog, using `${name}` as syslog tag.
#
# @example from the README
#   rc_script { 'thelounge':
#     ensure        => 'present',
#     user          => 'user',
#     group         => 'group',
#     cwd           => '/srv/thelounge',
#     env           => { 'THELOUNGE_HOME' => '/srv/thelounge'}
#     daemonize     => true,
#     use_syslog    => true,
#     restart_delay => 30, # seconds,
#     command       => '/usr/local/bin/thelounge',
#     command_args  => 'start',
#   }
define rc_script (
  Enum['present', 'absent'] $ensure   = 'present',
  Optional[String] $user              = undef,
  Optional[String] $group             = undef,
  Optional[String] $daemon_class      = undef,
  Optional[String] $limits            = undef,
  Array[String] $requires             = [],
  Hash[String, String] $env           = {},
  Optional[Stdlib::Absolutepath] $cwd = undef,
  Boolean $daemonize                  = false,
  Optional[Integer] $restart_delay    = undef,
  Boolean $use_syslog                 = false,
  Optional[String] $command_args      = undef,
  String[1] $description              = "${name} daemon",
  Stdlib::Absolutepath $command,
) {

  if $restart_delay and !$daemonize {
    fail('restart_delay requires to put command under control of daemon(8)')
  }

  if $use_syslog and !$daemonize {
    fail('use_syslog requires to put command under control of daemon(8)')
  }

  $location = $facts['os']['family']? {
    'FreeBSD' => '/usr/local/etc/rc.d',
    default   => '/etc/rc.d',
  }

  $rcd = {
    'name'          => $name,
    'description'   => $description,
    'command'       => $command,
    'command_args'  => $command_args,
    'user'          => $user,
    'group'         => $group,
    'daemon_class'  => $daemon_class,
    'limits'        => $limits,
    'requires'      => $requires,
    'env'           => $env,
    'cwd'           => $cwd,
    'daemonize'     => $daemonize,
    'restart_delay' => $restart_delay,
    'use_syslog'    => $use_syslog,
  }

  file { $title:
    ensure  => $ensure,
    path    => "${location}/${name}",
    mode    => '0755',
    owner   => 'root',
    group   => 'wheel',
    content => epp('rc_script/rc.daemon.epp', $rcd),
  }
}
